//
//  AppDelegate.h
//  NFLData
//
//  Created by Ian Copeman on 02/03/2021.
//

#import <Cocoa/Cocoa.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;


@end

